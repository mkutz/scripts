while [[ $# -gt 1 ]]
do
	key="$1"

	case $key in
		-t|--time)
			TIME="$2"
			shift # past argument
			;;
		-d|--distance)
			DISTANCE="$2"
			shift # past argument
			;;
		*)
			# unknown option
			;;
	esac
	shift # past argument or value
done

echo ran for $TIME minutes, ${DISTANCE}km on `date +"%A the %d/%m"` >> ~/running
echo wow!!! for $TIME minutes?! AMAZINGGGG!!!!!
