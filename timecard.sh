#!/bin/bash

verbs=("start" "stop")
file_path="/home/mkutz/timecard"

function filter {
	for i in ${verbs[@]}
		do echo $i
	done | grep -v $1
}

function echo_date {
	echo $1 `date +"%d/%m %H:%M"` >> $file_path
	echo $1 'working now'
}

function needed {
        prev=`tail -n 1 ${file_path} | cut -f 1 -d " "`
        filter $prev
}

if [ $1 ]
then
	if [ -e "$file_path" ]
	then
		if [ $1 == "$(needed)" ]
	       	then
			echo_date $1
		else
                        echo "you must $(needed) the session before $1ing"
		fi
	else 
		if [[ $1 == "start" ]]
	       	then
			echo_date $1
		else
			echo "You have not started a timecard yet"
			exit
		fi
	fi
else
	if [ -e "$file_path" ]
	then
		echo_date $(needed) 
	else
		echo_date "start"
	fi
fi
